import { BrowserRouter, Route, Routes } from "react-router-dom";
import Header from "./components/general/Header";
import ListadoDoctores from "./components/Doctores/ListadoDoctores";
import  FormularioDoctores from "./components/Doctores/FormularioDoctores";
import Home from "./components/Home/Home";

function App() {
  return (
    <div className="App">
      <Header/>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home/>} exact/>
          <Route path="/doctores" element={<ListadoDoctores/>} exact/>
          <Route path="/form/doctores" element={<FormularioDoctores/>} exact/>
          <Route path="/form/doctores/:id" element={<FormularioDoctores/>} exact/>
        </Routes>
      </BrowserRouter>
    </div>
  );
}
export default App;